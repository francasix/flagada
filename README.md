# Flagada API #

Flagada API manages Flags, Stamps and Colours used in Memory Stamps project.

## Config ##

`/config/db.config.js`

```
module.exports = {
    HOST: "xx.xxx.xx.xxx",
    USER: "my_user",
    PASSWORD: "my_password"
    DB: "db_name",
    dialect: "mysql",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};
```

## Installation ##

launch mysql

### Commands ###

#### Locally
```
yarn start
```

#### With docker

First of all you need to specify environment `MYSQL_ROOT_PASSWORD` in docker-compose file.

```
sudo docker-compose -f docker-compose.yml up -d --build
```
