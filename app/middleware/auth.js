const accessTokenSecret = 'thisisafuckingaccesstokensecret';
const refreshTokenSecret = 'thisisafuckingrefreshtokensecret';
const axios = require('axios');

module.exports = {
  isTokenValid: (req, res, next) => {
    const refreshToken = req.body.refreshToken;

    if (refreshToken) {

      axios.post(`http://localhost:8080/api/token/name`, { name: refreshToken }).then((response) => {
        if (!response.data.length) {
          next();
        } else {
          // Token blacklisted
          return res.status(403).send({
            msg: 'Token unauthorized',
            redirect: '/'
          });
        }
      });
    } else {
      return res.status(401).send({
        msg: 'Token is missing',
        redirect: '/'
      });
    }
  }
};