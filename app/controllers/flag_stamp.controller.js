const db = require('../models');
const Flag_stamp = db.flag_stamp;
const Op = db.Sequelize.Op;

// Create and Save a new Flag_stamp
exports.create = (req, res) => {
  // Validate request
  if (!req.body.flag_id) {
    res.status(400).send({
      message: 'Flag id is missing'
    });
    return;
  }

  if (!req.body.stamp_id) {
    res.status(400).send({
      message: 'Stamp id is missing'
    });
    return;
  }

  const flag_stamp = {
    flag_id: req.body.flag_id,
    stamp_id: req.body.stamp_id
  };

  // Save Flag_stamp in the database
  Flag_stamp.create(flag_stamp)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while creating the Flag_stamp.'
      });
    });
};

// Retrieve all Flag_stamps from the database.
exports.findAll = (req, res) => {
  const title = req.query.title;
  const condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Flag_stamp.findAll({ attributes: ['flag_id', 'stamp_id', 'id'], where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while retrieving flag_stamps.'
      });
    });
};

// Find a single Flag_stamp with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  if (!id) {
    res.status(400).send({
      message: 'Id is missing'
    });
    return;
  }

  Flag_stamp.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: 'Error retrieving Flag_stamp with id=' + id
      });
    });
};

// Find all Flag_stamp by the id in the request
exports.findAllByFlagId = (req, res) => {

  const flag_id = req.params.id;
  const condition = { flag_id: { [Op.like]: `%${flag_id}%` } };

  Flag_stamp.findAll({ attributes: ['flag_id', 'stamp_id', 'id'], where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: 'Error retrieving Flag_stamp with id=' + flag_id
      });
    });
};

// Delete a Flag_stamp with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Flag_stamp.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num === 1) {
        res.send({
          message: 'Flag_stamp was deleted successfully!'
        });
      } else {
        res.send({
          message: `Cannot delete Flag_stamp with id=${id}. Maybe Flag_stamp was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: 'Could not delete Flag_stamp with id=' + id
      });
    });

};

// Delete all Flag_stamps from the database.
exports.deleteAll = (req, res) => {
  Flag_stamp.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Flag_stamps were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while removing all flag_stamps.'
      });
    });
};