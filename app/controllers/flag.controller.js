const db = require('../models');
const Flag = db.flags;
const Op = db.Sequelize.Op;

// Create and Save a new Flag
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({
      message: 'Content can not be empty!'
    });
    return;
  }

  const flag = {
    title: req.body.title
  };

  // Save Flag in the database
  Flag.create(flag)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while creating the Flag.'
      });
    });
};

// Retrieve all Flags from the database.
exports.findAll = (req, res) => {
  const title = req.query.title;
  const condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Flag.findAll({ attributes: ['title', 'id'], where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while retrieving flags.'
      });
    });
};

// Retrieve all Flags from the database.
exports.findAllContaining = (req, res) => {
  const { title } = req.body;

  const condition = { title: { [Op.substring]: `%${title}%` } };

  Flag.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while retrieving stamps.'
      });
    });
};

// Find a single Flag with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Flag.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: 'Error retrieving Flag with id=' + id
      });
    });
};

// Update a Flag by the id in the request
exports.update = (req, res) => {

};

// Delete a Flag with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Flag.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num === 1) {
        res.send({
          message: 'Flag was deleted successfully!'
        });
      } else {
        res.send({
          message: `Cannot delete Flag with id=${id}. Maybe Flag was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: 'Could not delete Flag with id=' + id
      });
    });

};

// Delete all Flags from the database.
exports.deleteAll = (req, res) => {
  Flag.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Flags were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while removing all flags.'
      });
    });
};