const db = require('../models');
const Stamp = db.stamps;
const Op = db.Sequelize.Op;

// Create and Save a new Stamp
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({
      message: 'Title must be specified!'
    });
    return;
  }

  if (!req.body.colour_id) {
    res.status(400).send({
      message: 'Colour must be specified!'
    });
    return;
  }

  const stamp = {
    title: req.body.title,
    colour_id: req.body.colour_id
  };

  // Save Stamp in the database
  Stamp.create(stamp)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while creating the Stamp.'
      });
    });
};

// TODO Create bulk of Stamps at once
exports.createBulk = (req, res) => {

  req.body.stamps.forEach((stamp) => Stamp.findOne({
    where: {
      title: {
        [Op.like]: stamp.title
      },
      colour_id: {
        [Op.eq]: stamp.colour_id
      }
    }
  }));

  Stamp.bulkCreate(req.body.stamps)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while creating bulk of stamps.'
      });
    });
};

// Retrieve all Stamps from the database.
exports.findAll = (req, res) => {
  const title = req.query.title;
  const condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Stamp.findAll({ attributes: ['title', 'colour_id', 'id'], where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while retrieving stamps.'
      });
    });
};

// Retrieve all Stamps by list of Ids.
exports.findAllByIDs = (req, res) => {
  const { ids } = req.body;
  const condition = { id: ids };

  if (!req.body.ids) {
    res.status(400).send({
      message: 'Ids must be specified!'
    });
    return;
  }

  Stamp.findAll({ attributes: ['title', 'colour_id', 'id'], where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while retrieving stamps by ids.'
      });
    });
};

// Retrieve all Stamps from the database.
exports.findAllContaining = (req, res) => {
  const { title } = req.body;

  const condition = { title: { [Op.substring]: `%${title}%` } };

  Stamp.findAll({ attributes: ['title', 'colour_id', 'id'], where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while retrieving stamps.'
      });
    });
};

// Find a single Stamp with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Stamp.findByPk(id, { attributes: ['title', 'colour_id', 'id'] })
    .then(data => {
      res.send(data);
    })
    .catch(() => {
      res.status(500).send({
        message: 'Error retrieving Stamp with id=' + id
      });
    });
};

exports.findOrCreate = (req, res) => {
  const condition = { title: req.body.title, colour_id: req.body.colour_id };

  Stamp.findOrCreate({ attributes: ['title', 'colour_id', 'id'], where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(() => {
      res.status(500).send({
        message: 'Error while finding or creating Stamp'
      });
    });
};

// Delete a Stamp with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Stamp.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num === 1) {
        res.send({
          message: 'Stamp was deleted successfully!'
        });
      } else {
        res.send({
          message: `Cannot delete Stamp with id=${id}. Maybe Stamp was not found!`
        });
      }
    })
    .catch(() => {
      res.status(500).send({
        message: 'Could not delete Stamp with id=' + id
      });
    });

};

// Delete all Stamps from the database.
exports.deleteAll = (req, res) => {
  Stamp.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Stamps were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while removing all stamps.'
      });
    });
};