const db = require('../models');
const Token = db.token;

// Create and Save a new Token
exports.create = (req, res) => {
  // Validate request
  if (!req.body.token) {
    res.status(400).send({
      message: 'Token must be specified!'
    });
    return;
  }

  const token = {
    name: req.body.token
  };

  // Save Token in the database
  Token.create(token)
    .then(data => {
      res.send({ token: data.token });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while creating the Token.'
      });
    });
};

// Find a single Token with an email
exports.findByName = (req, res) => {
  const { name } = req.body;
  const condition = { name };

  Token.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(() => {
      res.status(500).send({
        message: 'Error retrieving Token'
      });
    });
};

// Delete all Tokens from the database.
exports.deleteAll = (req, res) => {
  Token.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Tokens were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while removing all tokens.'
      });
    });
};