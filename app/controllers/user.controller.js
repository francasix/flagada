const db = require('../models');
const User = db.user;
const Op = db.Sequelize.Op;

// Create and Save a new User
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({
      message: 'Name must be specified!'
    });
    return;
  }

  if (!req.body.email) {
    res.status(400).send({
      message: 'Email must be specified!'
    });
    return;
  }

  if (!req.body.password) {
    res.status(400).send({
      password: 'Email must be specified!'
    });
    return;
  }

  const user = {
    name: req.body.name,
    password: req.body.password,
    email: req.body.email
  };

  // Save User in the database
  User.create(user)
    .then(data => {
      res.send({ name: data.name, email: data.email, id: data.id});
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while creating the User.'
      });
    });
};

// Retrieve all Users from the database.
exports.findAll = (req, res) => {
  const name = req.query.name;
  const condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

  User.findAll({ attributes: ['name', 'email', 'id'], where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while retrieving users.'
      });
    });
};

// Find a single User with an email
exports.findByEmail = (req, res) => {
  const { email } = req.body;
  const condition = { email };

  User.findAll({ attributes: ['name', 'email', 'id', 'password'], where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(() => {
      res.status(500).send({
        message: 'Error retrieving User with email=' + email
      });
    });
};

exports.findOrCreate = (req, res) => {
  const condition = { name: req.body.name, email: req.body.email };

  User.findOrCreate({ attributes: ['email', 'email', 'id'], where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(() => {
      res.status(500).send({
        message: 'Error while finding or creating User'
      });
    });
};

// Delete a User with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  User.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num === 1) {
        res.send({
          message: 'User was deleted successfully!'
        });
      } else {
        res.send({
          message: `Cannot delete User with id=${id}. Maybe User was not found!`
        });
      }
    })
    .catch(() => {
      res.status(500).send({
        message: 'Could not delete User with id=' + id
      });
    });

};

// Delete all Users from the database.
exports.deleteAll = (req, res) => {
  User.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Users were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while removing all users.'
      });
    });
};