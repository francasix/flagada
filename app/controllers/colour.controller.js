const db = require('../models');
const Colour = db.colours;
const Op = db.Sequelize.Op;

// Create and Save a new Colour
exports.create = (req, res) => {
  // Validate request
  if (!req.body.luminance) {
    res.status(400).send({
      message: 'Luminance can not be empty!'
    });
    return;
  }

  if (!req.body.hexacode) {
    res.status(400).send({
      message: 'Hexacode can not be empty!'
    });
    return;
  }

  const colour = {
    hexacode: req.body.hexacode,
    luminance: req.body.luminance
  };

  // Save Colour in the database
  Colour.create(colour)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while creating the Colour.'
      });
    });
};

// Retrieve all Colours from the database.
exports.findAll = (req, res) => {
  const title = req.query.title;
  const condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Colour.findAll({ attributes: ['hexacode', 'luminance', 'id'], where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while retrieving colours.'
      });
    });
};

// Find a single Colour with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Colour.findByPk(id, { attributes: ['hexacode', 'luminance', 'id'] })
    .then(data => {
      res.send(data);
    })
    .catch(() => {
      res.status(500).send({
        message: 'Error retrieving Colour with id=' + id
      });
    });
};

exports.findOrCreate = (req, res) => {
  const condition = { hexacode: req.body.hexacode, luminance: req.body.luminance };

  Colour.findOrCreate({ attributes: ['hexacode', 'luminance', 'id'], where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(() => {
      res.status(500).send({
        message: 'Error while finding or creating Colour'
      });
    });
};

// Delete a Colour with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Colour.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num === 1) {
        res.send({
          message: 'Colour was deleted successfully!'
        });
      } else {
        res.send({
          message: `Cannot delete Colour with id=${id}. Maybe Colour was not found!`
        });
      }
    })
    .catch(() => {
      res.status(500).send({
        message: 'Could not delete Colour with id=' + id
      });
    });

};

// Delete all Colours from the database.
exports.deleteAll = (req, res) => {
  Colour.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Colours were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while removing all colours.'
      });
    });
};