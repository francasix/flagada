const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('../config/options.config.js');

const app = express();

const db = require('./models');
db.sequelize.sync({ force: config.forceSync }).then(() => {
  console.log('Drop and re-sync db.');
});

var corsOptions = {
  origin: config.corsOrigin
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get('/', (req, res) => {
  res.json({ message: 'Welcome to flagada API.' });
});

require('./routes/flag.routes')(app);
require('./routes/stamp.routes')(app);
require('./routes/colour.routes')(app);
require('./routes/flag_stamp.routes')(app);
require('./routes/user.routes')(app);
require('./routes/token.routes')(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});