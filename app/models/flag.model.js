module.exports = (sequelize, Sequelize) => {
  return sequelize.define('flag', {
    title: {
      type: Sequelize.STRING,
      allowNull: false
    }
  });
};