module.exports = (sequelize, Sequelize) => {
  return sequelize.define('flag_stamp', {
    flag_id: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    stamp_id: {
      type: Sequelize.INTEGER,
      allowNull: false
    }
  });
};