module.exports = (sequelize, Sequelize) => {
  return sequelize.define('stamp', {
    title: {
      type: Sequelize.STRING,
      allowNull: false
    },
    colour_id: {
      type: Sequelize.INTEGER,
      allowNull: false
    }
  });
};