module.exports = (sequelize, Sequelize) => {
  return sequelize.define('colour', {
    hexacode: {
      type: Sequelize.STRING,
      allowNull: false
    },
    luminance: {
      type: Sequelize.STRING,
      allowNull: false
    }
  });
};