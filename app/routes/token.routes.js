module.exports = app => {
  const token = require('../controllers/token.controller.js');

  let router = require('express').Router();

  // Create a new Token
  router.post('/', token.create);

  // Retrieve a single Token
  router.post('/name', token.findByName);

  // Delete all Token
  router.delete('/', token.deleteAll);

  app.use('/api/token', router);
};