const middleWare = require('../middleware/auth');

module.exports = app => {
  const flags = require('../controllers/flag.controller.js');

  let router = require('express').Router();

  // Create a new Flag
  router.post('/', flags.create);

  // Retrieve all Flags
  router.get('/', flags.findAll);

  // Retrieve all Flags with title containing
  router.get('/title/:query?', flags.findAllContaining);

  // Retrieve a single Flag with id
  router.get('/:id', flags.findOne);

  // Update a Flag with id
  router.put('/:id', flags.update);

  // Delete a Flag with id
  router.delete('/:id', flags.delete);

  // Delete all Flags
  router.delete('/', flags.deleteAll);

  app.use('/api/flags', router);
};