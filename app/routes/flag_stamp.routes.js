module.exports = app => {
  const flag_stamps = require('../controllers/flag_stamp.controller.js');

  let router = require('express').Router();

  // Create a new item
  router.post('/', flag_stamps.create);

  // Retrieve all items
  router.get('/', flag_stamps.findAll);

  // Retrieve a single item with id
  router.get('/:id', flag_stamps.findOne);

  // Retrieve all items with flag_id
  router.get('/flag/:id', flag_stamps.findAllByFlagId);

  // Delete a item with id
  router.delete('/:id', flag_stamps.delete);

  // Delete all items
  router.delete('/', flag_stamps.deleteAll);

  app.use('/api/flag_stamps', router);
};