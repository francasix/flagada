module.exports = app => {
  const colours = require('../controllers/colour.controller.js');

  let router = require('express').Router();

  // Create a new Colour
  router.post('/', colours.create);

  // Retrieve all Colours
  router.get('/', colours.findAll);

  // Retrieve a single Colour with id
  router.get('/:id', colours.findOne);

  // Find or Create Colour
  router.put('/', colours.findOrCreate);

  // Delete a Colour with id
  router.delete('/:id', colours.delete);

  // Delete all Colours
  router.delete('/', colours.deleteAll);

  app.use('/api/colours', router);
};