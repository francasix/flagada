module.exports = app => {
  const stamps = require('../controllers/stamp.controller.js');

  let router = require('express').Router();

  // Create a new Stamp
  router.post('/', stamps.create);

  // Create several new Stamps
  router.post('/bulk', stamps.createBulk);

  // Retrieve all Stamps
  router.get('/', stamps.findAll);

  // Retrieve all Stamps by a list of IDs
  router.post('/ids', stamps.findAllByIDs);

  // Retrieve all Stamps with title containing
  router.get('/title/:query?', stamps.findAllContaining);

  // Retrieve a single Stamp with id
  router.get('/:id', stamps.findOne);

  // Delete a Stamp with id
  router.delete('/:id', stamps.delete);

  // Delete all Stamps
  router.delete('/', stamps.deleteAll);

  // Find or Create Stamp
  router.put('/', stamps.findOrCreate);

  app.use('/api/stamps', router);
};