module.exports = app => {
  const users = require('../controllers/user.controller.js');

  let router = require('express').Router();

  // Create a new User
  router.post('/', users.create);

  // Retrieve all Users
  router.get('/', users.findAll);

  // Retrieve a single User with email
  router.post('/email', users.findByEmail);

  // Delete a User with id
  router.delete('/:id', users.delete);

  // Delete all Users
  router.delete('/', users.deleteAll);

  // Find or Create User
  router.put('/', users.findOrCreate);

  app.use('/api/users', router);
};